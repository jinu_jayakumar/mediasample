package com.jinu.mediasample.task;


import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

import com.jinu.mediasample.databases.SongsTableHelper;

/**
 * Created by jinu on 30/12/2016.
 **/

public class SyncAlbumTask extends BaseSyncTask<Void, Void, Void> {


    public SyncAlbumTask(ContentResolver context, GetResponse response) {
        super(context, response);
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        response.onResponse();
        super.onPostExecute(aVoid);
    }

    @Override
    protected Void doInBackground(Void... params) {
        String[] projection = new String[]{
                MediaStore.Audio.Albums._ID,
                MediaStore.Audio.Albums.ALBUM,
                MediaStore.Audio.Albums.ARTIST,
                MediaStore.Audio.Albums.NUMBER_OF_SONGS,
                MediaStore.Audio.Albums.ALBUM_ART,
                MediaStore.Audio.Albums.NUMBER_OF_SONGS};
        String sortOrder = MediaStore.Audio.Media.ALBUM + " COLLATE NOCASE ASC";
        Uri uri = MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI;
        Cursor cursor = contentResolver.get().query(uri, projection, null, null, sortOrder);
        assert cursor != null;
        SongsTableHelper.insertMusic(contentResolver.get(), cursor);
        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }
}
