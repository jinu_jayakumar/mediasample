package com.jinu.mediasample.task;

import android.content.ContentResolver;
import android.os.AsyncTask;

import java.lang.ref.WeakReference;

/**
 * Created by jinu on 30/12/2016.
 **/

public abstract class BaseSyncTask<V, V1, V2> extends AsyncTask<Void, Void, Void> {

    WeakReference<ContentResolver> contentResolver;
    final GetResponse response;

    public interface GetResponse {
        void onResponse();
    }

    BaseSyncTask(ContentResolver contentResolver, GetResponse response) {
        this.contentResolver = new WeakReference<>(contentResolver);
        this.response = response;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
    }

    @Override
    protected Void doInBackground(Void... params) {
        return null;
    }
}
