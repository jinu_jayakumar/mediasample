package com.jinu.mediasample.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by jinu on 20/12/2016.
 **/

public class MusicModel implements Parcelable {

    public String title;
    public String album_id;
    public String data;
    public String location;
    public String image;
    public String artist;
    public String length;
    public String genre;
    public String date_added;
    public String album;
    public String year;


    public MusicModel() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.album_id);
        dest.writeString(this.data);
        dest.writeString(this.location);
        dest.writeString(this.image);
        dest.writeString(this.artist);
        dest.writeString(this.length);
        dest.writeString(this.genre);
        dest.writeString(this.date_added);
        dest.writeString(this.album);
        dest.writeString(this.year);
    }

    protected MusicModel(Parcel in) {
        this.title = in.readString();
        this.album_id = in.readString();
        this.data = in.readString();
        this.location = in.readString();
        this.image = in.readString();
        this.artist = in.readString();
        this.length = in.readString();
        this.genre = in.readString();
        this.date_added = in.readString();
        this.album = in.readString();
        this.year = in.readString();
    }

    public static final Creator<MusicModel> CREATOR = new Creator<MusicModel>() {
        @Override
        public MusicModel createFromParcel(Parcel source) {
            return new MusicModel(source);
        }

        @Override
        public MusicModel[] newArray(int size) {
            return new MusicModel[size];
        }
    };
}
