package com.jinu.mediasample.interfaces;

/**
 * Created by jinu on 21/12/2016.
 **/

public interface OnSortListener {

    void onSort(String query);

}
