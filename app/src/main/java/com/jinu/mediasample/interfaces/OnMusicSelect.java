package com.jinu.mediasample.interfaces;

import android.support.v4.app.Fragment;

/**
 * Created by jinu on 20/12/2016.
 **/

public interface OnMusicSelect {

    void onSelectMusic(int musicId);

    void onReady(Fragment fragment);

}
