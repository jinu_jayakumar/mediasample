package com.jinu.mediasample.utils;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;

import com.jinu.mediasample.model.MusicModel;
import com.jinu.mediasample.service.MusicService;

/**
 * Created by jinu on 20/12/2016.
 **/

public class QueryUtils {


    public static Loader<Cursor> getAlbums(Context context) {
        String[] projection = new String[]{MediaStore.Audio.Albums._ID, MediaStore.Audio.Albums.ALBUM, MediaStore.Audio.Albums.ARTIST,
                MediaStore.Audio.Albums.ALBUM_ART, MediaStore.Audio.Albums.NUMBER_OF_SONGS};
        String sortOrder = MediaStore.Audio.Media.ALBUM + " COLLATE NOCASE ASC";
        return new CursorLoader(context, MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI, projection, null, null, sortOrder);

    }

    public static Loader<Cursor> getSongs(Context context) {
        String[] projection = new String[]{
                MediaStore.Audio.AudioColumns._ID,
                MediaStore.Audio.AudioColumns.TITLE,
                MediaStore.Audio.AudioColumns.ALBUM_ID,
                MediaStore.Audio.AudioColumns.ALBUM};
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String selection = MediaStore.Audio.AudioColumns.DURATION + " > ?";
        String[] selectionArgs = new String[]{"90000"};
        return new CursorLoader(context, uri, projection, selection, selectionArgs, null);
    }

    public static int getMusicId(int position, CursorAdapter adapter) {
        Cursor cursor = (Cursor) adapter.getItem(position);
        return cursor.getInt(cursor.getColumnIndex(MediaStore.Audio.AudioColumns._ID));
    }

    public static Intent playMusic(int musicId, Context context) {
        Intent intent = new Intent(context, MusicService.class);
        intent.putExtra(MediaStore.Audio.AudioColumns._ID, musicId);
        context.startService(intent);
        return intent;
    }

    public static Uri getMusicUri(ContentResolver resolver, Intent intent, MusicModel mMusicModel) {
        int albumId = intent.getIntExtra(MediaStore.Audio.AudioColumns._ID, 0);
        try {
            String[] projection = new String[]{MediaStore.Audio.AudioColumns.DATA,
                    MediaStore.Audio.AudioColumns.TITLE,
                    MediaStore.Audio.AudioColumns.ALBUM};
            String selection = MediaStore.Audio.AudioColumns._ID + " = ?";
            String[] selArgs = new String[]{String.valueOf(albumId)};
            Cursor cursor = resolver.query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, projection, selection, selArgs, null);
            assert cursor != null;
            if (cursor.moveToFirst()) {
                String uri = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.AudioColumns.DATA));
                mMusicModel.location = uri;
                mMusicModel.album = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.AudioColumns.ALBUM));
                mMusicModel.title = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.AudioColumns.TITLE));
                mMusicModel.album_id = String.valueOf(albumId);
                cursor.close();
                return Uri.parse(uri);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public static Loader<Cursor> getGenre(Context context) {


        String[] STAR = {MediaStore.Audio.Genres._ID,
                MediaStore.Audio.Genres.NAME};


        String query = " _id in (select genre_id from audio_genres_map where audio_id in (select _id from audio_meta where is_music != 0))";

        return new CursorLoader(context,
                MediaStore.Audio.Genres.EXTERNAL_CONTENT_URI, STAR, query,
                null, null);
    }

    public static Loader<Cursor> getAtrist(Context context) {
      Uri uri=  MediaStore.Audio.Artists.EXTERNAL_CONTENT_URI;
        return new CursorLoader(context,
               uri, null, null,
                null, null);
    }
}
