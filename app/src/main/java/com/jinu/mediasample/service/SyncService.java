package com.jinu.mediasample.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.jinu.mediasample.activity.StartActivity;
import com.jinu.mediasample.task.SyncAlbumTask;

public class SyncService extends Service implements SyncAlbumTask.GetResponse {
    public SyncService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (intent != null) {
            syncMusic();
        }

        return super.onStartCommand(intent, flags, startId);
    }

    private void syncMusic() {
        new SyncAlbumTask(getContentResolver(),this).execute();

    }

    @Override
    public void onResponse() {
        sendBroadcast(new Intent(StartActivity.KEY_SYNC_MUSIC));
        syncArtist();
        //stopSelf();
    }

    private void syncArtist() {

    }
}
