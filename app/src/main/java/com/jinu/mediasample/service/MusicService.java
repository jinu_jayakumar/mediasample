package com.jinu.mediasample.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.ResultReceiver;
import android.support.v4.media.RatingCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.util.Log;

import com.jinu.mediasample.activity.MainActivity;
import com.jinu.mediasample.model.MusicModel;
import com.jinu.mediasample.utils.QueryUtils;
import com.jinu.mediasample.utils.Utils;

import java.io.IOException;

public class MusicService extends Service implements MediaPlayer.OnCompletionListener, MediaPlayer.OnPreparedListener {

    private static final String TAG = "MusicService";
    private MediaPlayer mediaPlayer;
    private IBinder mBinder = new MyBinder();
    private MusicModel mMusicModel;
    private AudioManager audioManager;
    private BroadcastReceiver mNoicyBoradCast;
    private MediaSessionCompat mediaSessionCompat;
    private AudioManager.OnAudioFocusChangeListener mAudioListener = new AudioManager.OnAudioFocusChangeListener() {
        @Override
        public void onAudioFocusChange(int focusChange) {
            if (AudioManager.AUDIOFOCUS_LOSS == focusChange) {
                closePlayer();
            } else if (AudioManager.AUDIOFOCUS_LOSS_TRANSIENT == focusChange) {
                pausePlayer();
            } else if (AudioManager.AUDIOFOCUS_GAIN == focusChange) {
                mediaPlayer.start();
            } else if (AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK == focusChange) {
                // TODO: 21/12/2016 this will call when you want to play notification sound lower the music vol
            }
        }
    };

    private void pausePlayer() {
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.pause();
            unregisterReceiver(mNoicyBoradCast);
            mediaSessionCompat.setActive(false);
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public void onCreate() {
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setOnCompletionListener(this);
        mHandler = new Handler();
        mMusicModel = new MusicModel();
        mediaSessionCompat = new MediaSessionCompat(this, TAG);
        mediaSessionCompat.setFlags(MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS |
                MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS);
        mediaSessionCompat.setCallback(new MediaCallReciver());
        mNoicyBoradCast = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                pausePlayer();
            }
        };
        audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand() called with: intent = [" + intent +
                "], flags = [" + flags + "], startId = [" + startId + "]");
        if (intent != null) {
            if (intent.getExtras().getBoolean("change_seek")) {
                try {
                    int progress = intent.getExtras().getInt("progress");
                    mediaPlayer.pause();
                    mediaPlayer.seekTo(mediaPlayer.getDuration() * progress / 100);
                    mediaPlayer.start();
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
                return START_STICKY;

            } else {
                if (playMusic(intent)) {
                    return START_NOT_STICKY;
                }
            }

        } else {
            stopSelf();
            return START_NOT_STICKY;
        }

        return START_STICKY;
    }

    private boolean playMusic(Intent intent) {
        try {
            mediaPlayer.reset();
            Uri uri = QueryUtils.getMusicUri(getContentResolver(), intent, mMusicModel);
            if (uri == null) {
                return true;
            }
            mHandler.removeCallbacks(mUpdateTimeTask);
            mediaPlayer.setDataSource(this, uri);
            mediaPlayer.setOnPreparedListener(this);
            int result = audioManager.requestAudioFocus(mAudioListener,
                    AudioManager.STREAM_MUSIC,
                    AudioManager.AUDIOFOCUS_GAIN);
            if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
                mediaPlayer.prepare();
                if (!mediaPlayer.isPlaying()) {
                    addBroadCast();
                    mediaPlayer.start();
                    mediaSessionCompat.setActive(true);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    private void addBroadCast() {
        IntentFilter intentFilter = new IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY);
        registerReceiver(mNoicyBoradCast, intentFilter);
    }

    @Override
    public void onDestroy() {
        closePlayer();
        audioManager.abandonAudioFocus(mAudioListener);
        mediaPlayer.release();
    }

    private void closePlayer() {
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
            unregisterReceiver(mNoicyBoradCast);
            mediaSessionCompat.setActive(false);
        }
    }


    @Override
    public void onCompletion(MediaPlayer _mediaPlayer) {
        stopSelf();
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        mediaPlayer.start();
        updateProgressBar();
    }


    /**
     * Update timer on seekbar
     */
    public void updateProgressBar() {
        mHandler.postDelayed(mUpdateTimeTask, 1000);
    }

    private Handler mHandler;
    /**
     * Background Runnable thread
     */
    private Runnable mUpdateTimeTask = new Runnable() {

        public void run() {
            long currentDuration = mediaPlayer.getCurrentPosition() / 1000;
            long totalPosition = mediaPlayer.getDuration() / 1000;
            int percentage = Utils.getProgressPercentage(currentDuration, totalPosition);
            Message message = new Message();
            Bundle bundle = new Bundle();
            bundle.putLong("length", currentDuration);
            bundle.putLong("total", totalPosition);
            bundle.putLong("percent", percentage);
            bundle.putString("title", mMusicModel.title);
            bundle.putString("album", mMusicModel.album);
            message.setData(bundle);
            try {
                Intent intent = new Intent(MainActivity.ACTION_ON_PLAYER_UPDATE);
                intent.putExtras(bundle);
                sendBroadcast(intent);
            } catch (Exception e) {
                e.printStackTrace();
            }
            mHandler.postDelayed(this, 1000);

        }
    };

    public class MyBinder extends Binder {
        public MusicService getService() {
            return MusicService.this;
        }
    }


    private class MediaCallReciver extends MediaSessionCompat.Callback {

        public MediaCallReciver() {
            super();
        }

        @Override
        public void onCommand(String command, Bundle extras, ResultReceiver cb) {
            super.onCommand(command, extras, cb);
        }

        @Override
        public boolean onMediaButtonEvent(Intent mediaButtonEvent) {
            return super.onMediaButtonEvent(mediaButtonEvent);
        }

        @Override
        public void onPrepare() {
            super.onPrepare();
        }

        @Override
        public void onPrepareFromMediaId(String mediaId, Bundle extras) {
            super.onPrepareFromMediaId(mediaId, extras);
        }

        @Override
        public void onPrepareFromSearch(String query, Bundle extras) {
            super.onPrepareFromSearch(query, extras);
        }

        @Override
        public void onPrepareFromUri(Uri uri, Bundle extras) {
            super.onPrepareFromUri(uri, extras);
        }

        @Override
        public void onPlay() {
            super.onPlay();
        }

        @Override
        public void onPlayFromMediaId(String mediaId, Bundle extras) {
            super.onPlayFromMediaId(mediaId, extras);
        }

        @Override
        public void onPlayFromSearch(String query, Bundle extras) {
            super.onPlayFromSearch(query, extras);
        }

        @Override
        public void onPlayFromUri(Uri uri, Bundle extras) {
            super.onPlayFromUri(uri, extras);
        }

        @Override
        public void onSkipToQueueItem(long id) {
            super.onSkipToQueueItem(id);
        }

        @Override
        public void onPause() {
            super.onPause();
        }

        @Override
        public void onSkipToNext() {
            super.onSkipToNext();
        }

        @Override
        public void onSkipToPrevious() {
            super.onSkipToPrevious();
        }

        @Override
        public void onFastForward() {
            super.onFastForward();
        }

        @Override
        public void onRewind() {
            super.onRewind();
        }

        @Override
        public void onStop() {
            super.onStop();
        }

        @Override
        public void onSeekTo(long pos) {
            super.onSeekTo(pos);
        }

        @Override
        public void onSetRating(RatingCompat rating) {
            super.onSetRating(rating);
        }

        @Override
        public void onCustomAction(String action, Bundle extras) {
            super.onCustomAction(action, extras);
        }
    }
}
