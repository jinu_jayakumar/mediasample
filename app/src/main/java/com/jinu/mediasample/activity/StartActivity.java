package com.jinu.mediasample.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;

import com.jinu.mediasample.R;
import com.jinu.mediasample.service.MusicService;
import com.jinu.mediasample.service.SyncService;
import com.jinu.mediasample.utils.Utils;

public class StartActivity extends AppCompatActivity {


    public static final String KEY_SYNC_MUSIC = "com.jinu.mediasample.SYNC_MUSIC";


    private static final int REQ_CODE_PERMISSIONS = 120;
    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            launchActivity();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        IntentFilter intentFilter = new IntentFilter(KEY_SYNC_MUSIC);
        registerReceiver(mReceiver, intentFilter);
        if (!Utils.isMyServiceRunning(MusicService.class, this)) {
            requestReadStorage();
        } else {
            launchActivity();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mReceiver);
    }

    private void requestReadStorage() {
        if (ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            String[] permissions = new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE};
            ActivityCompat.requestPermissions(this, permissions, REQ_CODE_PERMISSIONS);
            return;
        }
        syncMusic();

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQ_CODE_PERMISSIONS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                syncMusic();
            } else {
                finish();
            }
        }
    }

    public void syncMusic() {
        Intent intent = new Intent(this, SyncService.class);
        startService(intent);
    }

    private void launchActivity() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}
