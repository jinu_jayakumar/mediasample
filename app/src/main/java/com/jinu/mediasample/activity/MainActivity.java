package com.jinu.mediasample.activity;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.jinu.mediasample.R;
import com.jinu.mediasample.adapter.MusicViewPagerAdapter;
import com.jinu.mediasample.fragment.AlbumFragment;
import com.jinu.mediasample.fragment.ArtistFragment;
import com.jinu.mediasample.fragment.GenreFragment;
import com.jinu.mediasample.fragment.SongsFragment;
import com.jinu.mediasample.interfaces.OnMusicSelect;
import com.jinu.mediasample.service.MusicService;
import com.jinu.mediasample.utils.QueryUtils;
import com.jinu.mediasample.view.PlayerBottomView;


public class MainActivity extends AppCompatActivity implements OnMusicSelect, LoaderManager.LoaderCallbacks<Cursor> {

    private static final int REQ_CODE_PERMISSIONS = 120;
    private static final String TAG = "MainActivity";
    public static final String ACTION_ON_PLAYER_UPDATE = "action_on_player_update";
    private static final int ARTIST = 5;
    private static final int ALBUM = 6;
    private static final int GENRE = 7;
    private static final int SONGS = 8;
    private BottomSheetBehavior mBottomSheetBehavior;
    private PlayerBottomView mPlayerBottomView;
    boolean isBound = false;
    @SuppressWarnings("FieldCanBeLocal")
    private MusicService musicService;
    private SongsFragment songsFragment = new SongsFragment();
    private ArtistFragment artistFragment = new ArtistFragment();
    private AlbumFragment albumFragment = new AlbumFragment();
    private GenreFragment genreFragment = new GenreFragment();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        Toolbar top_app_interview = (Toolbar) findViewById(R.id.top_app_interview);
        ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);

        viewPager.setAdapter(new MusicViewPagerAdapter(getSupportFragmentManager(),
                songsFragment,
                artistFragment,
                albumFragment,
                genreFragment));
        tabLayout.setupWithViewPager(viewPager);
        setSupportActionBar(toolbar);
        top_app_interview.setNavigationIcon(R.drawable.ic_close_black_24dp);
        top_app_interview.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        });
        mPlayerBottomView = (PlayerBottomView) findViewById(R.id.bottom_sheet);
        mBottomSheetBehavior = BottomSheetBehavior.from(mPlayerBottomView);
        mBottomSheetBehavior.setBottomSheetCallback(callback);
        IntentFilter intentFilter = new IntentFilter(ACTION_ON_PLAYER_UPDATE);
        registerReceiver(broadcastReceiver, intentFilter);
        requestReadStorage();


    }


    private void requestReadStorage() {
        if (ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            String[] permissions = new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE};
            ActivityCompat.requestPermissions(this, permissions, REQ_CODE_PERMISSIONS);
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQ_CODE_PERMISSIONS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            }
        }
    }

    @Override
    public void onBackPressed() {
        if (mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            return;
        }
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_scrolling, menu);//Menu Resource, Menu
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_sort) {
            showSort();
        } else if (id == R.id.action_settings) {
            startActivity(new Intent(this, SearchActivity.class));
        } else if (id == R.id.action_equalizer) {
            Intent intent = new Intent();
            intent.setAction("android.media.action.DISPLAY_AUDIO_EFFECT_CONTROL_PANEL");
            if ((intent.resolveActivity(getPackageManager()) != null)) {
                startActivity(intent);
                Log.d(TAG, "onOptionsItemSelected: REQUEST_EQ is an int of your choosing");
                // REQUEST_EQ is an int of your choosing
            } else {
                Log.d(TAG, "onOptionsItemSelected: No equalizer found :(");
                // No equalizer found :(
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void showSort() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.sort_by);
        builder.setItems(R.array.array_list, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Log.d(TAG, "onClick() called with: dialog = [" + dialog + "], which = [" + which + "]");
                dialog.dismiss();
            }
        });
        builder.show();
    }

    @Override
    public void onSelectMusic(int musicId) {
        try {
            Intent intent = new Intent(this, MusicService.class);
            intent.putExtra(MediaStore.Audio.AudioColumns._ID, musicId);
            ComponentName componentName = startService(intent);
            bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onReady(Fragment fragment) {
        if (fragment instanceof ArtistFragment) {
            getSupportLoaderManager().initLoader(ARTIST, null, MainActivity.this);
        } else if (fragment instanceof AlbumFragment) {
            getSupportLoaderManager().initLoader(ALBUM, null, MainActivity.this);
        } else if (fragment instanceof GenreFragment) {
            getSupportLoaderManager().initLoader(GENRE, null, MainActivity.this);
        } else if (fragment instanceof SongsFragment) {
            getSupportLoaderManager().initLoader(SONGS, null, MainActivity.this);
        }

    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(broadcastReceiver);
        super.onDestroy();
    }


    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            mPlayerBottomView.changeMusic(intent);
        }
    };


    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            MusicService.MyBinder binder = (MusicService.MyBinder) service;
            musicService = binder.getService();
            isBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            isBound = false;
        }
    };

    private BottomSheetBehavior.BottomSheetCallback callback = new BottomSheetBehavior.BottomSheetCallback() {
        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {

        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            if (slideOffset > 0.36f) {
                findViewById(R.id.view_player_controller).setVisibility(View.GONE);
            } else {
                findViewById(R.id.view_player_controller).setVisibility(View.VISIBLE);
            }
        }
    };


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (id == ALBUM) {
            return QueryUtils.getAlbums(this);
        } else if (id == GENRE) {
            return QueryUtils.getGenre(this);
        } else if (id == ARTIST) {
            return QueryUtils.getAtrist(this);
        } else {
            return QueryUtils.getSongs(this);
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (loader.getId() == ARTIST) {
            artistFragment.onLoadFinished(data);
        } else if (loader.getId() == GENRE) {
            genreFragment.onLoadFinished(data);
        } else if (loader.getId() == SONGS) {
            songsFragment.onLoadFinished(data);
        } else if (loader.getId() == ALBUM) {
            albumFragment.onLoadFinished(data);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        if (loader.getId() == ARTIST) {
            artistFragment.onLoaderReset();
        } else if (loader.getId() == GENRE) {
            genreFragment.onLoaderReset();
        } else if (loader.getId() == SONGS) {
            songsFragment.onLoaderReset();
        } else if (loader.getId() == ALBUM) {
            albumFragment.onLoaderReset();
        }
    }


}
