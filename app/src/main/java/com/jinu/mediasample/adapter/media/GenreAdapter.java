package com.jinu.mediasample.adapter.media;

import android.content.Context;
import android.database.Cursor;
import android.provider.MediaStore;
import android.support.v4.widget.CursorAdapter;
import android.view.View;
import android.view.ViewGroup;

import com.jinu.mediasample.view.GenreView;

/**
 * Created by jinu on 27/12/2016.
 **/

public class GenreAdapter extends CursorAdapter {


    public GenreAdapter(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return new GenreView(context);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        GenreView genreView = (GenreView) view;
        genreView.setAlbumArtist(cursor.getString(cursor.getColumnIndex(MediaStore.Audio.GenresColumns.NAME)));
    }
}
