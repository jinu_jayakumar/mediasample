package com.jinu.mediasample.adapter.media;

import android.content.Context;
import android.database.Cursor;
import android.provider.MediaStore;
import android.support.v4.widget.CursorAdapter;
import android.view.View;
import android.view.ViewGroup;

import com.jinu.mediasample.view.AlbumView;

/**
 * Created by jinu on 19/12/2016.
 **/

public class AlbumAdapter extends CursorAdapter {



    public AlbumAdapter(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);

    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return new AlbumView(context);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        AlbumView albumView = (AlbumView) view;
        albumView.setAlbumArtist(cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Albums.ARTIST)));
        albumView.setAlbumName(cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Albums.ALBUM)));
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.AlbumColumns.ALBUM_ART));
        albumView.setAlbumImage(path);

    }
}
