package com.jinu.mediasample.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.jinu.mediasample.fragment.AlbumFragment;
import com.jinu.mediasample.fragment.ArtistFragment;
import com.jinu.mediasample.fragment.GenreFragment;
import com.jinu.mediasample.fragment.SongsFragment;

/**
 * Created by jinu on 18/12/2016.
 **/

public class MusicViewPagerAdapter extends FragmentStatePagerAdapter {


    private final SongsFragment songsFragment;
    private final ArtistFragment artistFragment;
    private final AlbumFragment albumFragment;
    private final GenreFragment genreFragment;
    private String[] titles = new String[]{"Songs", "Artists", "Albums", "Genres"};

    public MusicViewPagerAdapter(FragmentManager fm, SongsFragment songsFragment,
                                 ArtistFragment artistFragment,
                                 AlbumFragment albumFragment, GenreFragment genreFragment) {
        super(fm);
        this.songsFragment = songsFragment;
        this.artistFragment = artistFragment;
        this.albumFragment = albumFragment;
        this.genreFragment = genreFragment;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return songsFragment;
        } else if (position == 1) {
            return artistFragment;
        } else if (position == 2) {
            return albumFragment;
        }
        return genreFragment;
    }

    @Override
    public int getCount() {
        return 4;
    }


    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }
}
