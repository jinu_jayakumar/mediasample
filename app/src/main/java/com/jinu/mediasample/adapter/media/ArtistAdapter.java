package com.jinu.mediasample.adapter.media;

import android.content.Context;
import android.database.Cursor;
import android.provider.MediaStore;
import android.support.v4.widget.CursorAdapter;
import android.view.View;
import android.view.ViewGroup;

import com.jinu.mediasample.view.ArtistView;

/**
 * Created by jinu on 27/12/2016.
 **/

public class ArtistAdapter extends CursorAdapter {


    public ArtistAdapter(Context context, Cursor c, boolean autoReQuery) {
        super(context, c, autoReQuery);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return new ArtistView(context);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ArtistView artistView = (ArtistView) view;
        artistView.setAlbumArtist(cursor.getString(cursor.getColumnIndex(MediaStore.Audio.ArtistColumns.ARTIST)));
    }
}
