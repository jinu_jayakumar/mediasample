package com.jinu.mediasample.adapter.media;

import android.content.Context;
import android.database.Cursor;
import android.provider.MediaStore;
import android.support.v4.app.LoaderManager;
import android.support.v4.widget.CursorAdapter;
import android.view.View;
import android.view.ViewGroup;

import com.jinu.mediasample.view.MediaView;

/**
 * Created by jinu on 16/12/2016.
 **/

public class SongsAdapter extends CursorAdapter {



    private final LoaderManager manager;

    public SongsAdapter(Context context, Cursor c, boolean autoRequery, LoaderManager manager) {
        super(context, c, autoRequery);
        this.manager = manager;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return new MediaView(context);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        MediaView mediaView = (MediaView) view;
        mediaView.setMediaName(cursor.getString(cursor.getColumnIndex(MediaStore.Audio.AudioColumns.TITLE)));
        mediaView.setMediaArtist(cursor.getString(cursor.getColumnIndex(MediaStore.Audio.AudioColumns.ALBUM)));
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.AlbumColumns.ALBUM_ID));
        mediaView.setMedia(path, manager);

    }
}
