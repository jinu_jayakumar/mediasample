package com.jinu.mediasample.databases;

import android.net.Uri;

import com.jinu.databasehelper.TableCreator;
import com.jinu.databasehelper.TableValues;

/**
 * Created by jinu on 20/12/2016.
 **/
@SuppressWarnings("WeakerAccess")
public class MediaTables {

    public static class SongsTable  implements  Musics {


        public static final String TABLE_NAME = "Songs";
        public static final Uri CONTENT_URI = Uri.parse("content://" + MusicContentProvider.AUTHORITY + "/" + TABLE_NAME);

        public static String createTable() {
            TableCreator tableCreator = new TableCreator(TABLE_NAME);
            tableCreator.addColumn(_ID, INTEGER, PRIMARY_KEY);
            tableCreator.addColumn(COL_TITLE, TEXT);
            tableCreator.addColumn(COL_ALBUM_ID, INTEGER);
            tableCreator.addColumn(COL_DATA, TEXT);
            tableCreator.addColumn(COL_IMAGE, TEXT);
            tableCreator.addColumn(COL_ARTIST, TEXT);
            tableCreator.addColumn(COL_LENGTH, TEXT);
            tableCreator.addColumn(COL_GENRE, TEXT);
            tableCreator.addColumn(COL_DATE_ADDED, DOUBLE);
            tableCreator.addColumn(COL_ALBUM, TEXT);
            tableCreator.addColumn(COL_YEAR, TEXT);
            tableCreator.addColumn(COL_ARTIST_ID, INTEGER);
            try {
                return tableCreator.create();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

    }



    public static class PlayListTable implements  Musics {

        public static final String TABLE_NAME = "PlayList";
        private static final String COL_IS_QUEUE = "is_queue";
        private static final Uri CONTENT_URI = Uri.parse("content://" + MusicContentProvider.AUTHORITY + "/" + TABLE_NAME);

        public static String createTable() {
            TableCreator tableCreator = new TableCreator(TABLE_NAME);
            tableCreator.addColumn(_ID, INTEGER, AUTO_PRIMARY_KEY);
            tableCreator.addColumn(COL_TITLE, TEXT);
            tableCreator.addColumn(COL_DATE_ADDED, DOUBLE);
            tableCreator.addColumn(COL_IS_QUEUE, BOOLEAN);
            tableCreator.addColumn(COL_YEAR, TEXT);
            try {
                return tableCreator.create();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

    }


     public static class AlbumTable implements MusicAlbums {


        public static final String TABLE_NAME = "Album";
        public static final Uri CONTENT_URI = Uri.parse("content://" + MusicContentProvider.AUTHORITY + "/" + TABLE_NAME);

         static String createTable() {
            TableCreator tableCreator = new TableCreator(TABLE_NAME);
            tableCreator.addColumn(_ID, INTEGER, PRIMARY_KEY);
            tableCreator.addColumn(COL_ALBUM_ID, INTEGER);
            tableCreator.addColumn(COL_ALBUM, TEXT);
            tableCreator.addColumn(COL_ARTIST, TEXT);
            tableCreator.addColumn(COL_IMAGE, TEXT);
            tableCreator.addColumn(COL_NUMBER_OF_SONGS, INTEGER);
            tableCreator.addColumn(COL_FIRST_YEAR, INTEGER);
            tableCreator.addColumn(COL_LAST_YEAR, INTEGER);
            tableCreator.addColumn(COL_ALBUM_KEY, TEXT);
            try {
                return tableCreator.create();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public static class ArtistTable implements ArtistMusics {


        public static final String TABLE_NAME = "Artist";
        private static final Uri CONTENT_URI = Uri.parse("content://" + MusicContentProvider.AUTHORITY + "/" + TABLE_NAME);

         static String createTable() {
            TableCreator tableCreator = new TableCreator(TABLE_NAME);
            tableCreator.addColumn(_ID, INTEGER, PRIMARY_KEY);
            tableCreator.addColumn(COL_ARTIST_KEY, TEXT);
            tableCreator.addColumn(COL_ARTIST, TEXT);
            tableCreator.addColumn(COL_NUMBER_OF_ALBUMS, INTEGER);
            tableCreator.addColumn(COL_NUMBER_OF_TRACKS, INTEGER);
            tableCreator.addColumn(COL_IMAGE, TEXT);
            try {
                return tableCreator.create();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }


     interface Musics extends BasicMusicColumns {

        String COL_TITLE = "title";
        String COL_DATA = "data";
        String COL_ARTIST = "artist";
        String COL_LENGTH = "length";
        String COL_GENRE = "genre";
        String COL_DATE_ADDED = "date_added";
        String COL_YEAR = "year";

    }


     interface BasicMusicColumns extends TableValues{
        String COL_IMAGE = "image";
        String COL_ALBUM_ID = "album_id";
        String COL_ALBUM = "album";
        String COL_ARTIST = "artist";
        String COL_ARTIST_ID = "artist_id";
    }


     interface MusicAlbums extends BasicMusicColumns {


        /**
         * The number of songs on this album
         * <P>Type: INTEGER</P>
         */
        String COL_NUMBER_OF_SONGS = "numsongs";

        /**
         * This column is available when getting album info via artist,
         * and indicates the number of songs on the album by the given
         * artist.
         * <P>Type: INTEGER</P>
         */
        String COL_NUMBER_OF_SONGS_FOR_ARTIST = "numsongs_by_artist";

        /**
         * The year in which the earliest songs
         * on this album were released. This will often
         * be the same as {@link #COL_LAST_YEAR}, but for compilation albums
         * they might differ.
         * <P>Type: INTEGER</P>
         */
        String COL_FIRST_YEAR = "minyear";

        /**
         * The year in which the latest songs
         * on this album were released. This will often
         * be the same as {@link #COL_FIRST_YEAR}, but for compilation albums
         * they might differ.
         * <P>Type: INTEGER</P>
         */
        String COL_LAST_YEAR = "maxyear";

        /**
         * A non human readable key calculated from the ALBUM, used for
         * searching, sorting and grouping
         * <P>Type: TEXT</P>
         */
        String COL_ALBUM_KEY = "album_key";


    }


    public interface ArtistMusics extends BasicMusicColumns{


        /**
         * A non human readable key calculated from the ARTIST, used for
         * searching, sorting and grouping
         * <P>Type: TEXT</P>
         */
        String COL_ARTIST_KEY = "artist_key";

        /**
         * The number of albums in the database for this artist
         */
        String COL_NUMBER_OF_ALBUMS = "number_of_albums";

        /**
         * The number of albums in the database for this artist
         */
         String COL_NUMBER_OF_TRACKS = "number_of_tracks";
    }
}
