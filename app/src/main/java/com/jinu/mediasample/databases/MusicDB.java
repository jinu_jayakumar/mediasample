package com.jinu.mediasample.databases;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by jinu on 20/12/2016.
 **/

class MusicDB extends SQLiteOpenHelper {


    private static final int VERSION = 1;
    private static final String NAME = "music.db";
    private static final String TAG = "MusicDB";

    @SuppressWarnings("UnusedParameters")
    MusicDB(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, NAME, factory, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(TAG, "onCreate() called with: db = [" + db + "]");
        db.execSQL(MediaTables.SongsTable.createTable());
        db.execSQL(MediaTables.PlayListTable.createTable());
        db.execSQL(MediaTables.AlbumTable.createTable());
        db.execSQL(MediaTables.ArtistTable.createTable());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
