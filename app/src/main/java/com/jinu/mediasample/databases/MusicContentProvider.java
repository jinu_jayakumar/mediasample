package com.jinu.mediasample.databases;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

/**
 * Created by jinu on 22/12/2016.
 **/

@SuppressWarnings("ConstantConditions")
public class MusicContentProvider extends ContentProvider {

    private MusicDB musicDB;

    public static final String AUTHORITY = "com.jinu.provider.music";
    private static final String DIR_TYPE = "vnd.android.cursor.dir/vnd";
    private static final String ELEMENT_TYPE = "vnd.android.cursor.item/vnd";
    private static final String COMPANY_NAME = "company_name";

    private static UriMatcher uriMatcher;

    public static final int SONGS_TABLE_ALL_ROWS = 1;
    public static final int SONGS_TABLE_SINGLE_ROW = 2;
    public static final int PLAY_LIST_TABLE_ALL_ROWS = 3;
    public static final int PLAY_LIST_TABLE_SINGLE_ROW = 4;
    public static final int ALBUM_TABLE_ALL_ROWS = 5;
    public static final int ALBUM_TABLE_SINGLE_ROW = 6;
    public static final int ARTIST_TABLE_ALL_ROWS = 7;
    public static final int ARTIST_TABLE_SINGLE_ROW = 8;


    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

        uriMatcher.addURI(AUTHORITY,
                MediaTables.SongsTable.TABLE_NAME, SONGS_TABLE_ALL_ROWS);

        uriMatcher.addURI(AUTHORITY,
                MediaTables.SongsTable.TABLE_NAME + "/#", SONGS_TABLE_SINGLE_ROW);

        uriMatcher.addURI(AUTHORITY,
                MediaTables.PlayListTable.TABLE_NAME, PLAY_LIST_TABLE_ALL_ROWS);

        uriMatcher.addURI(AUTHORITY,
                MediaTables.PlayListTable.TABLE_NAME + "/#", PLAY_LIST_TABLE_SINGLE_ROW);

        uriMatcher.addURI(AUTHORITY,
                MediaTables.AlbumTable.TABLE_NAME, ALBUM_TABLE_ALL_ROWS);

        uriMatcher.addURI(AUTHORITY,
                MediaTables.AlbumTable.TABLE_NAME + "/#", ALBUM_TABLE_SINGLE_ROW);

        uriMatcher.addURI(AUTHORITY,
                MediaTables.ArtistTable.TABLE_NAME, ARTIST_TABLE_ALL_ROWS);

        uriMatcher.addURI(AUTHORITY,
                MediaTables.ArtistTable.TABLE_NAME + "/#", ARTIST_TABLE_SINGLE_ROW);


    }


    @Override
    public boolean onCreate() {
        musicDB = new MusicDB(getContext(), null, null, 0);
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        SQLiteDatabase db = musicDB.getWritableDatabase();
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        String rowID = uri.getPathSegments().get(1);
        switch (uriMatcher.match(uri)) {
            case SONGS_TABLE_ALL_ROWS:
                queryBuilder.setTables(MediaTables.SongsTable.TABLE_NAME);
                break;
            case SONGS_TABLE_SINGLE_ROW:
                queryBuilder.setTables(MediaTables.SongsTable.TABLE_NAME);
                selection = MediaTables.SongsTable._ID + " = " + rowID +
                        ((!TextUtils.isEmpty(selection)) ? " AND ( " + selection + ")" : "");
                break;

            case PLAY_LIST_TABLE_ALL_ROWS:
                queryBuilder.setTables(MediaTables.PlayListTable.TABLE_NAME);
                break;
            case PLAY_LIST_TABLE_SINGLE_ROW:
                queryBuilder.setTables(MediaTables.PlayListTable.TABLE_NAME);
                selection = MediaTables.PlayListTable._ID + " = " + rowID +
                        ((!TextUtils.isEmpty(selection)) ? " AND ( " + selection + ")" : "");
                break;

            case ALBUM_TABLE_ALL_ROWS:
                queryBuilder.setTables(MediaTables.AlbumTable.TABLE_NAME);
                break;
            case ALBUM_TABLE_SINGLE_ROW:
                queryBuilder.setTables(MediaTables.AlbumTable.TABLE_NAME);
                selection = MediaTables.AlbumTable._ID + " = " + rowID +
                        ((!TextUtils.isEmpty(selection)) ? " AND ( " + selection + ")" : "");
                break;

            case ARTIST_TABLE_ALL_ROWS:
                queryBuilder.setTables(MediaTables.ArtistTable.TABLE_NAME);
                break;
            case ARTIST_TABLE_SINGLE_ROW:
                queryBuilder.setTables(MediaTables.ArtistTable.TABLE_NAME);
                selection = MediaTables.ArtistTable._ID + " = " + rowID +
                        ((!TextUtils.isEmpty(selection)) ? " AND ( " + selection + ")" : "");
                break;


        }
        return queryBuilder.query(db, projection, selection, selectionArgs, null, null, sortOrder);
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        switch (uriMatcher.match(uri)) {
            case SONGS_TABLE_ALL_ROWS:
                return DIR_TYPE + COMPANY_NAME + MediaTables.SongsTable.TABLE_NAME;

            case SONGS_TABLE_SINGLE_ROW:
                return ELEMENT_TYPE + COMPANY_NAME + MediaTables.SongsTable.TABLE_NAME;

            case PLAY_LIST_TABLE_ALL_ROWS:
                return DIR_TYPE + COMPANY_NAME + MediaTables.PlayListTable.TABLE_NAME;

            case PLAY_LIST_TABLE_SINGLE_ROW:
                return ELEMENT_TYPE + COMPANY_NAME + MediaTables.PlayListTable.TABLE_NAME;

            case ALBUM_TABLE_ALL_ROWS:
                return DIR_TYPE + COMPANY_NAME + MediaTables.AlbumTable.TABLE_NAME;

            case ALBUM_TABLE_SINGLE_ROW:
                return ELEMENT_TYPE + COMPANY_NAME + MediaTables.AlbumTable.TABLE_NAME;

            case ARTIST_TABLE_ALL_ROWS:
                return DIR_TYPE + COMPANY_NAME + MediaTables.ArtistTable.TABLE_NAME;

            case ARTIST_TABLE_SINGLE_ROW:
                return ELEMENT_TYPE + COMPANY_NAME + MediaTables.ArtistTable.TABLE_NAME;

        }
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        SQLiteDatabase db = musicDB.getWritableDatabase();
        long id = 0;
        switch (uriMatcher.match(uri)) {

            case SONGS_TABLE_ALL_ROWS:
                id = db.insert(MediaTables.SongsTable.TABLE_NAME, null, values);
                break;
            case SONGS_TABLE_SINGLE_ROW:
                id = db.insert(MediaTables.SongsTable.TABLE_NAME, null, values);
                break;

            case PLAY_LIST_TABLE_ALL_ROWS:
                id = db.insert(MediaTables.PlayListTable.TABLE_NAME, null, values);
                break;
            case PLAY_LIST_TABLE_SINGLE_ROW:
                id = db.insert(MediaTables.PlayListTable.TABLE_NAME, null, values);
                break;

            case ALBUM_TABLE_ALL_ROWS:
                id = db.insert(MediaTables.AlbumTable.TABLE_NAME, null, values);
                break;
            case ALBUM_TABLE_SINGLE_ROW:
                id = db.insert(MediaTables.AlbumTable.TABLE_NAME, null, values);
                break;

            case ARTIST_TABLE_ALL_ROWS:
                id = db.insert(MediaTables.ArtistTable.TABLE_NAME, null, values);
                break;
            case ARTIST_TABLE_SINGLE_ROW:
                id = db.insert(MediaTables.ArtistTable.TABLE_NAME, null, values);
                break;

        }
        if (id > 1) {
            Uri insertID = ContentUris.withAppendedId(uri, id);
            getContext().getContentResolver().notifyChange(insertID, null);
            return insertID;
        }
        return null;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        SQLiteDatabase db = musicDB.getWritableDatabase();
        int id = 0;
        switch (uriMatcher.match(uri)) {

            case SONGS_TABLE_ALL_ROWS:
                id = db.delete(MediaTables.SongsTable.TABLE_NAME, selection, selectionArgs);
                break;
            case SONGS_TABLE_SINGLE_ROW:
                id = db.delete(MediaTables.SongsTable.TABLE_NAME, selection, selectionArgs);
                break;

            case PLAY_LIST_TABLE_ALL_ROWS:
                id = db.delete(MediaTables.PlayListTable.TABLE_NAME, selection, selectionArgs);
                break;
            case PLAY_LIST_TABLE_SINGLE_ROW:
                id = db.delete(MediaTables.PlayListTable.TABLE_NAME, selection, selectionArgs);
                break;

            case ALBUM_TABLE_ALL_ROWS:
                id = db.delete(MediaTables.AlbumTable.TABLE_NAME, selection, selectionArgs);
                break;
            case ALBUM_TABLE_SINGLE_ROW:
                id = db.delete(MediaTables.AlbumTable.TABLE_NAME, selection, selectionArgs);
                break;

            case ARTIST_TABLE_ALL_ROWS:
                id = db.delete(MediaTables.ArtistTable.TABLE_NAME, selection, selectionArgs);
                break;
            case ARTIST_TABLE_SINGLE_ROW:
                id = db.delete(MediaTables.ArtistTable.TABLE_NAME, selection, selectionArgs);
                break;

        }
        if (id > 1) {
            Uri insertID = ContentUris.withAppendedId(uri, id);
            getContext().getContentResolver().notifyChange(insertID, null);
            return id;
        }
        return 0;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        SQLiteDatabase db = musicDB.getWritableDatabase();
        long id = 0;
        switch (uriMatcher.match(uri)) {

            case SONGS_TABLE_ALL_ROWS:
                id = db.update(MediaTables.SongsTable.TABLE_NAME, values, selection, selectionArgs);
                break;
            case SONGS_TABLE_SINGLE_ROW:
                id = db.update(MediaTables.SongsTable.TABLE_NAME, values, selection, selectionArgs);
                break;

            case PLAY_LIST_TABLE_ALL_ROWS:
                id = db.update(MediaTables.PlayListTable.TABLE_NAME, values, selection, selectionArgs);
                break;
            case PLAY_LIST_TABLE_SINGLE_ROW:
                id = db.update(MediaTables.PlayListTable.TABLE_NAME, values, selection, selectionArgs);
                break;

            case ALBUM_TABLE_ALL_ROWS:
                id = db.update(MediaTables.AlbumTable.TABLE_NAME, values, selection, selectionArgs);
                break;
            case ALBUM_TABLE_SINGLE_ROW:
                id = db.update(MediaTables.AlbumTable.TABLE_NAME, values, selection, selectionArgs);
                break;

            case ARTIST_TABLE_ALL_ROWS:
                id = db.update(MediaTables.ArtistTable.TABLE_NAME, values, selection, selectionArgs);
                break;
            case ARTIST_TABLE_SINGLE_ROW:
                id = db.update(MediaTables.ArtistTable.TABLE_NAME, values, selection, selectionArgs);
                break;

        }
        if (id > 1) {
            Uri insertID = ContentUris.withAppendedId(uri, id);
            getContext().getContentResolver().notifyChange(insertID, null);
            return (int) id;
        }
        return 0;
    }
}
