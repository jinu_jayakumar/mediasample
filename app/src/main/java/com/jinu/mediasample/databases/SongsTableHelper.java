package com.jinu.mediasample.databases;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.provider.MediaStore;

/**
 * Created by jinu on 20/12/2016.
 **/

public class SongsTableHelper {
    private static final String TAG = "SongsTableHelper";
    public static void insertMusic(ContentResolver contentResolver, Cursor cursor) {
        contentResolver.delete(MediaTables.AlbumTable.CONTENT_URI, null, null);
        if (cursor.moveToFirst()) {
            do {
                ContentValues values = new ContentValues();
                values.put(MediaTables.AlbumTable._ID,
                        cursor.getInt(cursor.getColumnIndex(MediaStore.Audio.Albums._ID)));
                values.put(MediaTables.AlbumTable.COL_ALBUM,
                        cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Albums.ALBUM)));
                values.put(MediaTables.AlbumTable.COL_ARTIST,
                        cursor.getInt(cursor.getColumnIndex(MediaStore.Audio.Albums.ARTIST)));
                values.put(MediaTables.AlbumTable.COL_IMAGE,
                        cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Albums.ALBUM_ART)));
                values.put(MediaTables.AlbumTable.COL_NUMBER_OF_SONGS,
                        cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Albums.NUMBER_OF_SONGS)));
                contentResolver.insert(MediaTables.AlbumTable.CONTENT_URI, values);
            } while (cursor.moveToNext());
        }
        cursor.close();
    }

    private static String getImage(String albumID, ContentResolver contentResolver) {

        Cursor cursor = contentResolver.query(MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI,
                new String[]{MediaStore.Audio.Albums._ID, MediaStore.Audio.Albums.ALBUM_ART},
                MediaStore.Audio.Albums._ID + "=?",
                new String[]{albumID}, null);
        assert cursor != null;
        if (cursor.moveToFirst()) {
            String image = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Albums.ALBUM_ART));
            cursor.close();
            return image;
        }
        return null;
    }


}
