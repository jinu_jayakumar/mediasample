package com.jinu.mediasample.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.jinu.mediasample.R;

/**
 * Created by jinu on 19/12/2016.
 **/

public class GenreView extends LinearLayout {


    protected ImageView mAlbumImage;
    protected TextView mAlbumArtist;


    public GenreView(Context context) {
        super(context);
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.adapter_genre, this);
        mAlbumImage = (ImageView) findViewById(R.id.genre_art);
        mAlbumArtist = (TextView) findViewById(R.id.genre_name);
    }

    public void setAlbumImage(String albumImage) {
        Glide.with(getContext())
                .load(albumImage)
                .override(100, 100)
                .centerCrop()
                .placeholder(R.drawable.ic_launcher)
                .into(mAlbumImage);
    }


    public void setAlbumArtist(String albumArtist) {
        mAlbumArtist.setText(albumArtist);
    }


}
