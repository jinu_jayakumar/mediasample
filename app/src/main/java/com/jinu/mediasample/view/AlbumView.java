package com.jinu.mediasample.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.jinu.mediasample.R;

/**
 * Created by jinu on 19/12/2016.
 **/

public class AlbumView extends LinearLayout {

    protected ImageView mAlbumImage;
    protected TextView mAlbumName;
    protected TextView mAlbumArtist;


    public AlbumView(Context context) {
        super(context);
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.adapter_album, this);
        mAlbumImage = (ImageView) findViewById(R.id.album_art);
        mAlbumArtist = (TextView) findViewById(R.id.album_name);
        mAlbumName = (TextView) findViewById(R.id.album_artist);
    }

    public void setAlbumImage(String albumImage) {
        Glide.with(getContext())
                .load(albumImage)
                .override(100, 100)
                .centerCrop()
                .placeholder(R.drawable.ic_launcher)
                .into(mAlbumImage);
    }

    public void setAlbumName(String albumName) {
        mAlbumName.setText(albumName);
    }

    public void setAlbumArtist(String albumArtist) {
        mAlbumArtist.setText(albumArtist);
    }
}
