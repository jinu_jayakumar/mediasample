package com.jinu.mediasample.view;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.jinu.mediasample.R;
import com.jinu.mediasample.service.MusicService;

/**
 * Created by jinu on 26/12/2016.
 **/

public class PlayerBottomView extends RelativeLayout implements View.OnClickListener {

    private ImageView mControllerPlay;
    private ImageView mControllerNext;
    private ImageView mControllerPrevious;
    private ImageView mPlayingPlay;
    private ImageView mPlayingNext;
    private ImageView mPlayingPrevois;
    private TextView mControllerTittle;
    private TextView mControllerSubTittle;
    private TextView mPayingTittle;
    private TextView mPlayingSubTittle;
    private TextView mCurrentPosition;
    private TextView mTotalPosition;
    private SeekBar mPlayingProgress;
    private SeekBar.OnSeekBarChangeListener mOnSeekListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            if (fromUser) {
                Intent intent = new Intent(getContext(), MusicService.class);
                intent.putExtra("change_seek", true);
                intent.putExtra("progress", progress);
                getContext().startService(intent);
            }
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };


    public PlayerBottomView(Context context) {
        super(context);

        init();
    }

    public PlayerBottomView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PlayerBottomView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }


    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.main_activity_bottom_view, this, true);
        mControllerTittle = (TextView) findViewById(R.id.media_name);
        mControllerSubTittle = (TextView) findViewById(R.id.media_display);
        mPayingTittle = (TextView) findViewById(R.id.media_name_now);
        mPlayingSubTittle = (TextView) findViewById(R.id.media_sub_now);
        mCurrentPosition = (TextView) findViewById(R.id.current_position);
        mTotalPosition = (TextView) findViewById(R.id.end_position);
        mPlayingProgress = (SeekBar) findViewById(R.id.progressBar);
        mControllerNext= (ImageView) findViewById(R.id.controller_next_btn);
        mControllerPrevious= (ImageView) findViewById(R.id.controller_previous_btn);
        mControllerPlay= (ImageView) findViewById(R.id.controller_play_pause_btn);
        mPlayingNext= (ImageView) findViewById(R.id.next_btn);
        mPlayingPrevois= (ImageView) findViewById(R.id.previous_btn);
        mPlayingPlay= (ImageView) findViewById(R.id.play_pause_btn);
        mPlayingProgress.setMax(100);
        mPlayingProgress.setProgress(0);
        mPlayingProgress.setOnSeekBarChangeListener(mOnSeekListener);
        setClickListener(mControllerNext,
                mControllerPrevious,
                mControllerPlay,
                mPlayingNext,
                mPlayingPrevois,
                mPlayingPlay);
    }

    private void setClickListener(ImageView... view) {
        for (ImageView imageView : view) {
            imageView.setOnClickListener(this);
        }
    }

    public void changeMusic(Intent intent) {
        if (intent == null) {
            return;
        }
        long length = intent.getExtras().getLong("length");
        long max = intent.getExtras().getLong("total");
        long percent = intent.getExtras().getLong("percent");

        if (length != 0 && max != 0) {
            mCurrentPosition.setText(length / 60 + ":" + length % 60);
            mTotalPosition.setText(max / 60 + ":" + max % 60);
            mPlayingProgress.setProgress((int) percent);
            mPayingTittle.setText(intent.getExtras().getString("title"));
            mControllerTittle.setText(intent.getExtras().getString("title"));
            mControllerSubTittle.setText(intent.getExtras().getString("album"));
            mPlayingSubTittle.setText(intent.getExtras().getString("album"));
        }
    }

    @Override
    public void onClick(View v) {

    }
}
