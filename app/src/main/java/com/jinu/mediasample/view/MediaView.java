package com.jinu.mediasample.view;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.jinu.mediasample.R;

/**
 * Created by jinu on 16/12/2016.
 **/

public class MediaView extends LinearLayout implements LoaderManager.LoaderCallbacks<Cursor> {

    protected TextView mediaName;
    protected TextView mediaArtist;
    protected ImageView mediaImage;
    private String media;


    public MediaView(Context context) {
        super(context);
        initView();
    }

    public MediaView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public MediaView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }


    protected void initView() {
        LayoutInflater.from(getContext()).inflate(R.layout.adapter_media, this);
        mediaName = (TextView) findViewById(R.id.media_name);
        mediaArtist = (TextView) findViewById(R.id.media_display);
        mediaImage = (ImageView) findViewById(R.id.image_media);
    }

    public void setMediaArtist(String mediaArtist) {
        this.mediaArtist.setText(mediaArtist);
    }

    public void setMediaName(String mediaName) {
        this.mediaName.setText(mediaName);
    }

    public void setMedia(String media, final LoaderManager supportLoaderManager) {
        this.media = media;
        clearImage();
        Runnable runnable = new Runnable() {
            public void run() {
                supportLoaderManager.restartLoader(2, null, MediaView.this);
            }
        };
        new Handler().postDelayed(runnable,60);

    }

    private void loadMedia(String path) {
        Glide.with(getContext())
                .load(path)
                .override(100, 100)
                .centerCrop()
                .placeholder(R.drawable.ic_launcher)
                .into(mediaImage);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getContext(), MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI,
                new String[]{MediaStore.Audio.Albums._ID, MediaStore.Audio.Albums.ALBUM_ART},
                MediaStore.Audio.Albums._ID + "=?",
                new String[]{media},
                null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        if (cursor.moveToFirst()) {
            String path = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Albums.ALBUM_ART));
            try {
                loadMedia(path);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        cursor.close();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        try {
            clearImage();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void clearImage() {
        loadMedia("");
    }
}
