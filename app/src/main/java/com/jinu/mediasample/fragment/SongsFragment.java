package com.jinu.mediasample.fragment;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.jinu.mediasample.R;
import com.jinu.mediasample.adapter.media.SongsAdapter;
import com.jinu.mediasample.interfaces.OnMusicSelect;
import com.jinu.mediasample.utils.QueryUtils;


public class SongsFragment extends Fragment implements AdapterView.OnItemClickListener {


    private ListView mListView;
    private SongsAdapter adapter;
    private static final String TAG = "SongsFragment";
    private OnMusicSelect onMusicSelect;


    public SongsFragment() {
        Log.d(TAG, "SongsFragment() called");
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        onMusicSelect = (OnMusicSelect) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_item_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mListView = (ListView) view.findViewById(R.id.list_of_media);
        adapter = new SongsAdapter(getActivity(), null, true, getActivity().getSupportLoaderManager());
        mListView.setAdapter(adapter);
        mListView.setOnItemClickListener(this);
        onMusicSelect.onReady(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        onMusicSelect.onSelectMusic(QueryUtils.getMusicId(position, adapter));
    }


    public void onLoadFinished(Cursor data) {
        if (adapter == null) {
            return;
        }
        adapter.swapCursor(data);
    }


    public void onLoaderReset() {
        if (adapter == null) {
            return;
        }
        adapter.swapCursor(null);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onMusicSelect = null;
    }
}
