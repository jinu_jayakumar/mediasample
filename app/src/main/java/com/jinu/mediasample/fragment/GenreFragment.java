package com.jinu.mediasample.fragment;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.jinu.mediasample.R;
import com.jinu.mediasample.adapter.media.GenreAdapter;
import com.jinu.mediasample.interfaces.OnMusicSelect;
import com.jinu.mediasample.utils.QueryUtils;

/**
 * Created by jinu on 19/12/2016.
 **/

public class GenreFragment extends Fragment implements AdapterView.OnItemClickListener {


    private GridView mListView;
    private GenreAdapter adapter;
    private static final String TAG = "GenreFragment";
    private OnMusicSelect onMusicSelect;


    public GenreFragment() {
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        onMusicSelect = (OnMusicSelect) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_album, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mListView = (GridView) view.findViewById(R.id.list_of_media);
        adapter = new GenreAdapter(getActivity(), null, true);
        mListView.setAdapter(adapter);
        mListView.setOnItemClickListener(this);
        onMusicSelect.onReady(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        onMusicSelect.onSelectMusic(QueryUtils.getMusicId(position, adapter));
    }


    public void onLoadFinished(Cursor data) {
        if (adapter == null) {
            return;
        }
        adapter.swapCursor(data);
    }


    public void onLoaderReset() {
        if (adapter == null) {
            return;
        }
        adapter.swapCursor(null);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onMusicSelect = null;
    }
}
