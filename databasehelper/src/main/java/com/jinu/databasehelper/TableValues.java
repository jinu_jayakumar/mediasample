package com.jinu.databasehelper;

import android.provider.BaseColumns;

/**
 * Created by jinu on 10/11/2016.
 **/

@SuppressWarnings("ALL")
public interface TableValues extends BaseColumns {

    int TEXT = 1;
    int INTEGER = 2;
    int BOOLEAN = 3;
    int DATE = 4;
    int DATETIME = 5;
    int FLOAT = 6;
    int DOUBLE = 7;
    int PRIMARY_KEY = 8;
    int AUTO_PRIMARY_KEY = 9;
    int NOT_NULL = 10;
    int DEFAULT = 11;
    int UNIQUE = 12;

}
