package com.jinu.databasehelper;

/**
 * Created by jinu on 10/11/2016.
 **/

public final class TableCreator extends TableCreatorAbs {


    public TableCreator(String table) {
        super(table);
    }

    @Override
    public void addColumn(String colName, int colType, int colConstrain) {
        if (isNotNull(colName) || isNotNull(colType)) {
            throw new NullPointerException(" colName or colTYpe is empty");
        }
        addColumnOnTable(colName, colType, colConstrain);
    }




    @Override
    public void addColumn(String colName, int colType, int colConstrain, String defValue) {
        addColumn(colName, colType, colConstrain);
        append(DEFAULT + defValue);
    }


    @Override
    public void addColumn(String colName, int colType) {
        addColumn(colName, colType, 0);
    }

    /**
     * returns text to create table
     */
    public String create() throws Exception {
        if (!mAddedRow) {
            throw new NullPointerException("You need to add a new row first");
        }
        return getValue() + ")";
    }


}
