package com.jinu.databasehelper;

/**
 * Created by jinu on 13/11/2016.
 **/
abstract class TableCreatorAbs {


    private static final String TEXT = "TEXT";
    private static final String INTEGER = "INTEGER";
    private static final String BOOLEAN = "BOOLEAN";
    private static final String DATE = "DATE";
    private static final String DATETIME = "DATETIME";
    private static final String FLOAT = "FLOAT";
    private static final String DOUBLE = "DOUBLE";
    private static final String PRIMARY_KEY = "PRIMARY KEY";
    private static final String NOT_NULL = "NOT NULL";
    static final String DEFAULT = "DEFAULT";
    private static final String UNIQUE = "UNIQUE";
    private static final String CREATE_TABLE = "CREATE TABLE ";
    private static final String AUTO_INCREMENT = " AUTOINCREMENT";

    boolean mAddedRow = false;
    private StringBuilder mBuilder;

    TableCreatorAbs(String table) {
        mBuilder = new StringBuilder(CREATE_TABLE);
        append(table);
    }

    void append(String table) {
        mBuilder.append(table);
    }


    /**
     * converts integer value to text equivalent
     */
    private String getColType(int colType) {
        if (colType == TableValues.INTEGER) {
            return INTEGER;
        } else if (colType == TableValues.TEXT) {
            return TEXT;
        } else if (colType == TableValues.BOOLEAN) {
            return BOOLEAN;
        } else if (colType == TableValues.DATE) {
            return DATE;
        } else if (colType == TableValues.DATETIME) {
            return DATETIME;
        } else if (colType == TableValues.FLOAT) {
            return FLOAT;
        } else {
            return DOUBLE;
        }
    }

     void addColumnOnTable(String colName, int colType, int colConstrain) {
        addSeparation();
        addNewColumn(colName, colType);
        addConstraint(colType, colConstrain);
    }


    /**
     * adds  constraint
     *
     * @param colType type of column eg: TEXT,INTEGER
     * @param colConstrain extra values eg: AUTOINCREMENT,UNIQUE
     */
    private void addConstraint(int colType, int colConstrain) {
        if (!isNotNull(colConstrain) && !isPrimaryKey(colType)) {
            append(" ");
            append(getColConstraint(colConstrain));
        }
    }

    /**
     * @param colName name of the column
     * @param colType type of column eg: TEXT,INTEGER
     */
    private void addNewColumn(String colName, int colType) {
        append(colName);
        append(" ");
        append(getColType(colType));
    }

    /**
     * adds comma or open brace
     */
    private void addSeparation() {
        if (mAddedRow) {
            append(",");
        } else {
            mAddedRow = true;
            append("(");
        }
    }

    /**
     * converts integer value to text equivalent
     */
    private String getColConstraint(int colConstraint) {
        if (colConstraint == TableValues.PRIMARY_KEY) {
            return PRIMARY_KEY;
        } else if (colConstraint == TableValues.NOT_NULL) {
            return NOT_NULL;
        } else if (colConstraint == TableValues.DEFAULT) {
            return DEFAULT;
        } else if (colConstraint == TableValues.AUTO_PRIMARY_KEY) {
            return PRIMARY_KEY + AUTO_INCREMENT;
        } else {
            return UNIQUE;
        }
    }



    /**
     * checks if parameter is empty or not
     *
     * @param colType integer
     * @return true if null else true
     */
    boolean isNotNull(int colType) {
        return colType == 0;
    }

    /**
     * checks if parameter is empty or not
     *
     * @param str String
     * @return true if null else true
     */
    boolean isNotNull(String str) {
        return str == null || str.length() == 0;
    }



    /**
     * returns text to create table
     */
    String getValue() {
        return mBuilder.toString();
    }

    /**
     * checks if colType == PRIMARY_KEY
     */
    private boolean isPrimaryKey(int colType) {
        return TableValues.PRIMARY_KEY == colType;
    }

    /**
     * Creates String Based on parameter values
     *
     * @param colName      name of the column eg : _id
     * @param colType      type of column eg: TEXT,INTEGER
     * @param colConstrain extra values eg: AUTOINCREMENT,UNIQUE
     */
    public abstract void addColumn(String colName, int colType, int colConstrain);


    /**
     * Creates String Based on parameter values
     *
     * @param colName      name of the column eg : _id
     * @param colType      type of column eg: TEXT,INTEGER
     * @param colConstrain extra values eg: AUTOINCREMENT,UNIQUE
     * @param defValue     default value of column
     */
    public abstract void addColumn(String colName, int colType, int colConstrain, String defValue);

    /**
     * Creates String Based on parameter values
     *
     * @param colName name of the column eg : _id
     * @param colType type of column eg: TEXT,INTEGER
     */
    public abstract void addColumn(String colName, int colType);
}
