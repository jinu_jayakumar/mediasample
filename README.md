# Media Sample #

This Media Sample is a sample music playing application.


##Daily Progress updates   

###_20th dec 2016_  
- completed listing and bind by service to activity,  
- added play back of music by on click  
- bottom sheet integration is added   
- bottom sheet functionality added for back pressed and close btn  
- added new tables for further migration for own integrated db for playlist and other things  
- added external library database helper for easy table creation  
- moved item selection and starting service through an interface  
- added menu for search,filter,equalizer and help  
- landscape version of playlist is now added
- added splash screen bt not used will be updated tomorrow  
- created a utility class for query from the table


  
###_21st dec 2016_  
- bind complete of media to view through media player 
- now media title and sub title will show on playing 
- progress shows successfully
- accent color changed
- added listeners for headphone and other phone playback options
- design issue is now completed
- extracted media seekBar to another type
- added media button listener and its class in media service
- added search in your activity
- sort dialog on menu is now inserted



###_22nd dec 2016_  
- added new content provider 
- created a new service for sync data
- added uri for each tables
- added uri matcher for all fields and single elements 
- content provider insert delete update query for all 4 tables and 8 types are now created
- added album helper file and music sync is now complete
- insertion of songs table elements are completed
- removed music sync service and several other tables
- db helper updated 


###_26th dec 2016_  
- issue of music loading delay is now fixed
- moved fragment initialization to activity
- now media can be seekKed programmatically
- design and performance improvement
- separate fragment adapter added
- now listing of album and artist is successful
- created new two views for listing 
- all views are implemented
- improved launch time
- added launch time is controlled  if service is working


###_29th dec 2016_
- create album table
- create artist table
- added tables to db and content provider

###_14th may 2017_
- created new widget to play music from home screen(design implemented not yet implemented controls)

###_15th may 2017_


